<?php
class Upload {

  //Atributos
  private $_name;

     /*
  * Constructor: Inicia la subida del archivo
  * Entrada:
  *   $name: Nombre del elemento del formulario del que gestionamos el $_FILE
  */
    function __construct($name){
      $this->_name = $name;
      if (isset($_FILES[$name])) {
        $this->upload();
      } else {
        echo "no se ha subido";
      }
    }

  /*
  * upload: Función que hace las operaciones necesarias para subir el archivo
  * al servidor
  */
  public function upload(){
    $filename = $_FILES["photo"]["name"];
    move_uploaded_file($_FILES["photo"]["tmp_name"], "uploads/" . $filename);
  }

    /*
  * Getters. Lo que quiere decir que los atributos de la clase son private
  */
    public function getPath(){
//esta función nos tiene que dar la ruta del archivo
      return "uploads/" . $_FILES["photo"]["name"];
}

  /*
  * OPCIONAL:
  * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
  * en la subida de archivos. Por ejemplo:
  * throw new UploadError("Error: Please select a valid file format.");
  
  class UploadError extends Exception{}*/
}

?>