<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Galería</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

    <form method="POST" action="ficha_alumno_view.php" enctype="multipart/form-data">
        <label for="nom">Nombre:</label>
        <input id="nom" class="form-control" type="text" name="nombre">
        <label for="ap">Apellidos:</label>
        <input id="ap" class="form-control" type="text" name="apellidos">
        <label for="fot">Fotografía:</label>
        <input id="fot" class="form-control" type="file" name="photo" value="fileupload">
        <label for="dir">Dirección:</label>
        <input id="dir" class="form-control" type="text" name="direccion">
        <label for="com">Comentarios:</label>
        <input id="com" class="form-control" type="text" name="comentarios">
        <label for="ass">Asignaturas matriculadas:</label><br>
        <input type="checkbox" id="ass" name="asignaturas[]" value="programacion">
        <label for="as_1">Programación</label><br>
        <input type="checkbox" id="as_2" name="asignaturas[]" value="bbdd">
        <label for="as_2">Bases de Datos</label><br>
        <input type="checkbox" id="as_3" name="asignaturas[]" value="fol">
        <label for="as_3">FOL</label><br>

        <input class="form-control" type="submit" value="Upload">
    </form>

</body>

</html>