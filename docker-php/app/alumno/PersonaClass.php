<?php

class Persona {

    //atributes
    private $nombre;
    private $apellidos;
    private $foto;
    private $direccion;
    private $comentarios;
    private $asignaturas;

    //setters
    public function setName($name) {
        $this->nombre=$name;
    }

    public function setSurname($surname) {
        $this->apellidos=$surname;
    }

    public function setPicture($picture) {
        $this->foto=$picture;
    }

    public function setAddress($address) {
        $this->direccion=$address;
    }

    public function setComments($comments) {
        $this->comentarios=$comments;
    }

    public function setSubjects($subjects) {
        $this->asignaturas=$subjects;
    }

    //getters
    public function getName() {
        return $this->nombre;
    }

    public function getSurname() {
        return $this->apellidos;
    }

    public function getPhoto() {
        return $this->foto;
    }
    
    public function getAddress() {
        return $this->direccion;
    }

    public function getComments() {
        return $this->comentarios;
    }

    public function getSubjects() {
        return $this->asignaturas;
    }

}

?>