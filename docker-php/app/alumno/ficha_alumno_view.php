<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Ficha Alumno</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

<?php


    //import or "include" the needed classes to work
    include('UploadClass.php');
    include('PersonaClass.php');
    

    $upload = new Upload("photo");
    //print the path of the uploaded photo
    //echo $upload->getPath();

    //Create a new person & give him the attributes we got with POST
    $persona = new Persona();
    $persona->setName($_POST["nombre"]);
    $persona->setSurname($_POST["apellidos"]);
    $persona->setPicture($upload->getPath());
    $persona->setAddress($_POST["direccion"]);
    $persona->setComments($_POST["comentarios"]);

    //we put all the subjets together in a String to be printed
    $asignaturas = $_POST["asignaturas"];
    $todasAsignaturas;
    //print_r($asignaturas);
    //print($upload->getPath());

    for ($x = 0; $x < count($asignaturas); $x++) {
        $todasAsignaturas .= $asignaturas[$x] . ", ";
    }
    $persona->setSubjects($todasAsignaturas);
    ?>

<div class="card" style="width: 18rem;">        

        <img src="<?=$upload->getPath();?>" class="card-img-top">

        <div class="card-body">
        <p>Nombre: <?=$persona->getName();?></p>
        <p>Apellidos: <?=$persona->getSurname();?></p>
        <p>Direccion: <?=$persona->getAddress();?></p>
        <p>Comentarios: <?=$persona->getComments();?></p>
        <!-- salen las asignaturas pero quizá hay una manera mejor-->
        <p>Asignaturas: <?=$persona->getSubjects();?></p>
</div>



    </div>

</body>

</html>