<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Productos</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <style>
        .btn {
            width: 100%;
            margin-top: 1vh;
        }
    </style>
</head>

<body>
    <div class="container mt-3">

        <form method="POST" action="prod_list_02.php" class="mb-3">
            <label class="form-label">Número de productos:</label>
            <input class="form-control" type="text" name="productos">

            <input class="btn btn-success" type="submit" value="Submit">

        </form>
    </div>
</body>

</html>