<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Productos</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .btn {
            width: 100%;
            margin-top: 1vh;
        }

        .col-xs-2 {
            width: 50%;
        }
    </style>
</head>

<body>

    <div class="container">

        <form method="POST" action="prod_list_03.php">
            <?php
            //variables
            $prods = $_POST["productos"];

            //loop to create the new form

            for ($x = 0; $x < $prods; $x++) { ?>
                <div class="form-group row">
                    <div class="col-xs-2">

                        <label class="col-sm-2 control-label">Producto <?= $x + 1 ?>:</label>
                        <input class="form-control" id="ex1" type="text" name="nombres[]">
                    </div>
                    <div class="col-xs-2">

                        <label class="col-sm-2 control-label">Precio:</label>
                        <input class="form-control" type="text" name="precios[]">
                    </div>
                </div>

            <?php
            }
            ?>
            <input class="btn btn-success" type="submit" value="Submit">

        </form>
    </div>
</body>

</html>