<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Productos</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>
    <div class="container">

        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Producto</th>
                    <th>Precio</th>
                </tr>
            </thead>

            <?php
            //get the variables
            $nombres = $_POST["nombres"];
            $precios = $_POST["precios"];
            //lets introduce a var to count the products
            $index = 1;

            //loop to print the table
            for ($i = 0; $i < sizeof($nombres); $i++) {
            ?>

                <tr>
                    <td>
                        <!--índice de productos-->
                        <?php


                        //if else to check if there are products or prices

                        if (empty($nombres[$i]) || empty($precios[$i])) {
                            echo "Producto no introducido";
                        } else {
                            echo $index;
                            $index++;

                        ?>
                    </td>
                    <td>
                        <?= $nombres[$i] ?>
                    </td>
                    <td>
                        <?= $precios[$i] ?>

                    </td>
                </tr>
        <?php
                        }
                    }
        ?>
        </table>
    </div>

</body>

</html>