<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Pildora 3.1</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

<?php 

//variables
$loteria = array(61,32,43,61);
$numero = 61;
$ocurrencias = 0;

//comprobación de veces que aparece el número
$arrlength = count($loteria);

for($x = 0; $x < $arrlength; $x++) {
    if ($loteria[$x] == $numero) {
        $ocurrencias++;
    }
}

//mostramos el número
echo $ocurrencias;

?>
    
</body>

</html>