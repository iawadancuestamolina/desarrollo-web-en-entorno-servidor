<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>pildora 1.2</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <!--<style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 1px solid #8c8c8c;
        }

        th,
        td {
            text-align: left;
            padding: 16px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>-->
</head>

<body>

    <?php

    //declare variables
    $a = 6;
    $b = 3;
    ?>

    <table class="table table-hover">
        <tr>
            <th>Operación</th>
            <th>Valor</th>
        </tr>
        <tr>
            <td>A</td>
            <td><?php echo $a ?></td>
        </tr>
        <tr>
            <td>B</td>
            <td><?php echo $b ?></td>
        </tr>
        <tr>
            <td>A+B</td>
            <td><?php echo $a + $b ?></td>
        </tr>
        <tr>
            <td>A-B</td>
            <td><?php echo $a - $b ?></td>
        </tr>
        <tr>
            <td>A*B</td>
            <td><?php echo $a * $b ?></td>
        </tr>
        <tr>
            <td>A/B</td>
            <td><?php echo $a / $b ?></td>
        </tr>
        <tr>
            <td>AexpB</td>
            <td><?php echo $a ** $b ?></td>
        </tr>
    </table>
</body>

</html>