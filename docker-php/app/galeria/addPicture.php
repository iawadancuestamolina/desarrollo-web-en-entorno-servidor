<?php include_once('_header.php') ?>
<div class="card">
    <div class="card-body">
        <div class="bd-example">
            <form method="POST" action="uploadManager.php" enctype="multipart/form-data">
                <label for="titulo">Título:</label>
                <input id="titulo" type="text" name="titulo">
                <label for="imagen">Imagen:</label>
                <input id="imagen" type="file" name="foto" value="fileupload">
                <input id="submit" type="submit" value="Upload" class="btn btn-primary">
        </div>
    </div>
</div>
<?php include_once('_footer.php') ?>