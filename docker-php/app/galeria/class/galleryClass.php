<?php
class Gallery
{
    private $_gallery = [];
    private $_fileName = "listado.txt";

    /*Constructor: Recibe la ruta del archivo fotos.txt*/
    public function __construct()
    {
        $this->loadGallery();
    }

    /*
    *Recorre el archivo fotos.txt y para cada linea, crea un
    *elemento Picture que lo añade al atributo $_gallery[]
    */
    public function loadGallery()
    {
        //creamos una variable contador para el array $_gallery
        $j = 0;

        $myfile = fopen("listado.txt", "r") or die("Unable to open file!"); //abrimos el documento para leerlo

        while (!feof($myfile)) {     //mientras no llegue el fin del documento vamos leyendo línea a línea
            $linea = fgets($myfile);  //leemos una línea

            if($linea != "") {
            $token = strtok($linea, "###");     //hacemos un token de strings con titulos y paths
            $i = 0;
            while ($token !== false) {          //almacenamos el titulo y la ruta en un array mediante strtok()
                $tituloRuta[$i] = $token;
                $i++;
                $token = strtok("###");
            }


            $picture = new Picture($tituloRuta[0], $tituloRuta[1]); //creamos nuevo objeto Picture
            $this->_gallery[$j] = $picture;   //añadimos el objeto al array $_gallery[]
            $j++;                       //incrementamos $j para pasar al próximo elemento del array
            } 
        }
        fclose($myfile);    //cerramos el documento
    }

    /*
    *Getters.
    */
    public function getGallery()
    {
        return $this->_gallery;
    }
}
