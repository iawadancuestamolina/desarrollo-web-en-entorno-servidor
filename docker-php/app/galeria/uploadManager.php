<?php


/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["foto"])) uploadPicture();

function uploadPicture()
{
        $filename = $_FILES["foto"]["name"]; //coge el nombre de la imagen del campo "foto"

        //controlamos los errores
        //miramos si se ha subido bien
        try {
                if ($_FILES[$filename]["error"] != 0)
                        throw new UploadError("Error: " . $_FILES[$filename]["error"]);

                //archivos aceptados
                $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
                //tipo y tamaño del archivo
                $filetype = $_FILES[$filename]["type"];
                $filesize = $_FILES[$filename]["size"];

                //verificamos tipo de archivo
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                if (!array_key_exists($extension, $allowed))
                        throw new UploadError("Error: Debes subir un archivo jpg, jpeg, gif o png");

                //verificamos el tamaño (maximo 5 megas)
                $maxsize = 5 * 1024 * 1024;
                if ($filesize > $maxsize)
                        throw new UploadError("Error: El archivo pesa más de 5Mb");

                //verificamos si el archivo ya existe
                if (file_exists("uploads/" . $filename))
                        throw new UploadError("Error: El archivo " . $filename . " ya existe");

                //si no hay errores movemos la foto al lugar donde debe estar
                move_uploaded_file($_FILES["foto"]["tmp_name"], "uploads/" . $filename); //sube el archivo y lo mueve a "uploads/"
                addPictureToFile($filename, $_POST["titulo"]); //ejecuta la funcion que añade el nombre del input "titulo" y el archivo al .txt
                header("Location: index.php?upload=success"); //nos devuelve al index con un parámetro "upload=sucess"

        } catch (UploadError $e) {
                header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage())); //nos devuelve al index con un mensaje de error
                die();
        } catch (Exception $e) {
                header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));
                die();
        }
}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: null
*/
function addPictureToFile($file_uploaded, $title_uploaded)
{
        $myfile = fopen("listado.txt", "a") or die("Unable to open file!"); //abrimos el documento, si no existe se crea
        $line = $title_uploaded . "###uploads/" . $file_uploaded . "\n"; //escribimos una línea con la ruta y el titulo de la imagen
        fwrite($myfile, $line);  //cerramos el documento
        fclose($myfile);
}

/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.");
*/
class UploadError extends Exception
{
}
