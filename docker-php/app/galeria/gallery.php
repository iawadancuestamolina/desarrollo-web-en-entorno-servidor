<?php include_once('_header.php');

//importaciones
include('uploadManager.php');
include('class/pictureClass.php');
include('class/galleryClass.php');

//desplegamos la galería
//creamos un nuevo objeto galería
$galeria = new Gallery("listado.txt");

$album = $galeria->getGallery();    //almacenamos el array en una variable
?>

<div class="row" style="padding-top: 20px;">

<?php

//desplegamos la galería en html
//print_r($album);
foreach ($album as $elemento) {
 //  el ultimo \n lo arreglo con el -1 en el for
    ?>
    <div class="card" style="width: 30vw;">
        <img src="<?=$elemento->fileName()?>" alt="<?=$elemento->title()?>"  class="card-img-top">
        <div class="card-body">

        <p><?=$elemento->title()?></p>
</div>
</div>
<?php
}
    ?>
</div>

<?php include_once('_footer.php') ?>
