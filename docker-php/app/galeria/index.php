<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Galería</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

    <?php include_once('_header.php') ?>
    <div class="card">
        <?php
        $exito = $_GET['upload'];
        //redireccion si todo ha ido bien
        if ($exito == "success") {
        ?>

            <div class="alert alert-success">
                <p>Imagen subida correctamente!</p>
            </div>

        <?php
            //redireccion si hay errores
        } else if ($exito == "error") {
            $mensaje = $_GET['msg'];
        ?>
            <div class="alert alert-danger">
                <p><?= $mensaje ?></p>
            </div>
        <?php
        }
        ?>

        <div class="card-body">
            <div class="bd-example">
                <a type="button" class="btn btn-primary" href="addPicture.php">Add picture</a>
                <a type="button" class="btn btn-success" href="gallery.php">View Gallery</a>
            </div>
        </div>
    </div>
    <?php include_once('_footer.php') ?>

</body>

</html>