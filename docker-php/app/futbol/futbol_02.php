<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Futbol</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
        .btn {
            width: 100%;
            margin-top: 1vh;
        }
    </style>
</head>

<body>

    <div class="container mt-3">

        <form method="POST" action="futbol_03.php">

            <?php

            //variables
            $jugadores = $_POST["jugadores"];
            $partidos = $_POST["partidos"];

            //player & goals form building
            for ($x = 0; $x < $jugadores; $x++) { ?>

                <label>Introduce el nombre del jugador <?= $x + 1 ?>:</label>
                <input class="form-control" type="text" name="jugador[]">
                <?php
                for ($y = 0; $y < $partidos; $y++) { ?>
                    <label>Goles del partido <?= $y + 1 ?>:</label>
                    <input class="form-control" type="text" name="goles[<?= $x ?>][]">

            <?php
                }
            }
            ?>

            <input class="btn btn-success" type="submit" value="Submit">
    </div>
</body>

</html>