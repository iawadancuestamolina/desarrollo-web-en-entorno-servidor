<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Futbol</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>

    <div class="container mt-3">

        <table class="table table-striped">
            <thead>
                <tr>

                    <th>Jugador</th>





                    <?php

                    //variables
                    $jugadores = $_POST["jugador"];
                    $goles = $_POST["goles"];

                    //first header loop
                    for ($j = 0; $j < sizeof($goles[0]); $j++) {
                    ?>
                        <th>Partido <?= $j + 1 ?></th>
                    <?php
                    }
                    ?>
                </tr>
            </thead>

            <?php

            //create the loop
            for ($i = 0; $i < sizeof($jugadores); $i++) { ?>

                <tr>
                    <td>
                        <?= $jugadores[$i] ?>
                    </td>

                    <?php
                    for ($z = 0; $z < sizeof($goles[$i]); $z++) {
                    ?>
                        <td>
                            <?= $goles[$i][$z] ?>
                        </td>
                    <?php
                    }
                    ?>
                </tr>

            <?php


            }
            ?>





        </table>
    </div>

</body>

</html>