<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--TITULO-->
    <title>Pildora 3.2</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
</head>

<body>
<form method="POST" action="pildora3.2.php">
        <input class="form-control" type="text" name="num" />
        <input class="form-control" type="submit" value="Submit">
    </form>

<?php 

//variables
$loteria = array(61,32,43,61);
$numero = $_POST["num"];
$ocurrencias = 0;

//comprobación de veces que aparece el número
$arrlength = count($loteria);

for($x = 0; $x < $arrlength; $x++) {
    if ($loteria[$x] == $numero) {
        $ocurrencias++;
    }
}

//mostramos el número
echo "<h3>El número introducido aparece " . $ocurrencias . " veces en el array!</h3>";

?>
    
</body>

</html>